## What is it for ?
This is a bookmarklet to allow to exclude in one click all the categories app in a Google Ads account. To save time!

## How to use the bookmarklet
Copy the code of bookmarklet.js in a bookmark. Name it as you want.

1. In Google ads, go at the account settings level > Placements > Exclusions
2. Click to add exclusion
3. Just click on the last item "Categories, ..." in order to have Apple Apps and below Google Apps categories
4. Click on the bookmarlet. The categories tress are All the categories will be unfolded and checked. 
5. Just click on save.

Of course, you can keep some apps if you want.
